import numpy as np
import matplotlib.pyplot as plt
import random
from matplotlib.widgets import Button
from matplotlib import colors
import pandas as pd
from sympy import *

from gameboard import GameBoard
from basic import basicAlgo
from improved import improvedAlgo

# Main Function
def main():

    # Get input from terminal for board dimensions and number of mines
    try:
        dim = int(input("Please Enter the dimension: "))
        mines = int(input("Please Enter the number of mines: "))
    except ValueError:
        print("Invalid Entry.")
        exit(1)

    # Initialize the board.
    gameBoard = GameBoard(dim,mines)
    board = gameBoard.board

    printTerminalBoard(board)
    printGUI(board)
    #basicAlgo(gameBoard)
    #improvedAlgo(gameBoard)

    #print(pd.DataFrame(board))

    
    
    """
    a = np.array([[1,1,0], [1,1,1], [0,1,1]])
    b = np.array([2,1,1])

    X = np.linalg.inv(a).dot(b)
    print(X)
    """

    
    variables = symbols("a b c")
    equations = ['a+b', 'a+b+c', 'b+c']
    A,z = linear_eq_to_matrix(equations,variables)
    #z = Matrix([2,1,1])
    #print(linsolve((A,z), variables))
    V = np.array(A,dtype='float')
    b = [0,1,1]
    #print(B)

    #a = np.mat('[1 1 0 0 ; 1 1 1 0 ; 0 1 1 1 ; 0 0 1 1]')
    #b = np.mat('[1 ; 1 ; 2 ; 1]')
    #print(b)
    
    try:
        c = np.linalg.solve(V,b)
    except np.linalg.LinAlgError as err:
        print("invalid matrix")
    print(c)
    



# Prints the board to terminal
def printTerminalBoard(board):
    dim = len(board[0])
    for x in range(dim):
        for y in range(dim):
            print(board[x][y], end =" ")
        print()

# Prints the board on the GUI
def printGUI(board):
    cmap = colors.ListedColormap(['lightgreen', 'red', 'green'])
    bounds = [0,1,2]

    plt.imshow(board, cmap=cmap, vmin=0,vmax=2)

    bfsAX = plt.axes([0.001, 0.7, 0.15, 0.05])
    bfsBtn = Button(bfsAX, 'Next Step', color='lightgreen', hovercolor='green')
    bfsBtn.on_clicked(next_step)

    dfsAX = plt.axes([0.001, 0.6, 0.15, 0.05])
    dfsBtn = Button(dfsAX, 'Basic', color='lightgreen', hovercolor='green')
    dfsBtn.on_clicked(solve_basic)

    astar1AX = plt.axes([0.001, 0.5, 0.15, 0.05])
    astar1Btn = Button(astar1AX, 'Improved', color='lightgreen', hovercolor='green')
    astar1Btn.on_clicked(solve_improved)

    plt.show()

def next_step(e):
    print('next')

def solve_basic(e):
    print('basic')

def solve_improved(e):
    print('improved')

if __name__ == '__main__':
    main()




"""
Note : This won't work anymore with regular GameBoard class. This is meant to be called with
       a plain 2D list consisting of numbers -1,0,1 where -1 is a mine, 0 is covered, 1 is uncovered.
       Can easily transform GameBoard.board object into this type of array but no need for now

# TODO: Find better GUI or look into matplotlib more to accomplish this
# Prints abritrary board using matplotlib.
def printBoard(board):

    dim = len(board[0])
    cmap = colors.ListedColormap(['red', 'grey', 'green'])

    f, ax = plt.subplots()
    ax.imshow(board, cmap=cmap, vmin=-1,vmax=1)
    ax.set_frame_on(False)
    ax.set_yticklabels([])
    ax.set_xticklabels([])
    plt.show()
"""