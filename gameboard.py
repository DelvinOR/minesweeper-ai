import random
import numpy as np
# Class representing the gameboard environment. Consists of a 2D array of Cell objects
class GameBoard:

    # Fields
    dim = 0
    numMines = 0
    board = []
    
    # Constructor
    def __init__(self, dim, numMines):
        self.dim = dim
        self.numMines = numMines
        # Fills a board with empty slots
        self.board = [[0 for i in range(self.dim)] for j in range(self.dim)] 
        # Randomly generetes mines in the board
        i = 0
        while (i < self.numMines):
            x = random.randint(0,self.dim-1)
            y = random.randint(0,self.dim-1)
            
            if (self.board[y][x] == 1):
                continue
            self.board[y][x] = 1
            i += 1

    #returns the value of a specific cell
    def get(self, x, y):
        return self.board[x][y]
    
    # Given location, will return True if mine, False if not mine and number of mines surrounding it
    def checkLocation(self,x,y):
        if self.board[y][x] == 1:
            return True, -1
        else:
            surroundingMines = 0

            if (y-1 >= 0) and (x-1 >= 0) and (self.board[y-1][x-1] == 1): # top left
                surroundingMines += 1
            if (y-1 >= 0) and (self.board[y-1][x] == 1): # top
                surroundingMines += 1
            if (y-1 >= 0) and (x+1 < self.dim) and (self.board[y-1][x+1] == 1): # top right
                surroundingMines += 1
            if (x-1 >= 0) and (self.board[y][x-1] == 1): # left
                surroundingMines += 1
            if (x+1 < self.dim) and (self.board[y][x+1] == 1): # right
                surroundingMines += 1
            if (y+1 < self.dim) and (x-1 >= 0) and (self.board[y+1][x-1] == 1): # bottom left
                surroundingMines += 1
            if (y+1 < self.dim) and (self.board[y+1][x] == 1): # down
                surroundingMines += 1
            if (y+1 < self.dim) and (x+1 < self.dim) and (self.board[y+1][x+1] == 1): # bottom right
                surroundingMines += 1

            return False, surroundingMines

