from gameboard import GameBoard
import random

def improvedAlgo(gameBoard):
    board = gameBoard.board
    dim = len(board[0])

    blownUp = 0
    flagged = 0
    t = 0

    """
    List of dictionary values for knowledge base.
    Each cell has:
        cellLocation    <- [y,x] location of cell
        cellStatus      <- 'C' for covered, 'S' for safe, 'M' for mine
        cellClue        <- Once cell is opened, this is the number of mines around it
        numOpen         <- Number of squares identified around it
        numMines        <- Number of mines identified around it
        numHidden       <- Number of hidden squares around it
    """
    #kb = boardInit(board)
    # We will use the coordinates as the key for the dictionary
    kb = []

    # List of already searched cells
    searched = []

    # Main loop. Each cell will be evaluated once
    while True:

        # searched list represents all the cells we have looked at. Once all cells have been either
        # searched or flagged as a mine, we can stop
        if (len(searched) >= dim*dim):
            break

        chosenCell = None
        randomCheck = True

        # Pick a safe cell to search from KB that hasn't already been searched.
        for cell in kb:
            cellStatus = cell.get("cellStatus")
            cellLocation = cell.get("cellLocation")
            if (cellStatus == 'S') and (cellLocation not in searched):
                chosenCell = cell
                searched.append(cellLocation)
                randomCheck = False
                break

       # There are no safe cells available to us. Choose one at random.
        if(randomCheck):
            ranList = []
            while True:
                x = random.randint(0,dim-1)
                y = random.randint(0,dim-1)

                # Cell is not already searched.
                if ([y,x] not in ranList) and ([y,x] not in searched):
                    # We found a possible cell that we have to add to our knowledge base
                    # We add with empty values in the entry
                    chosenCell = {"cellLocation" : [y,x], "cellStatus" : 'C', "cellClue" : -1, "numOpen" : -1, "numMines" : -1, "numHidden" : -1}
                    kb.append(chosenCell)
                    search.append([y,x])
                    break
                else:
                    ranList.append([y,x])
        
        # Have cell we will explore, call the environment to give us the information about the cell
        coord = chosenCell.get("cellLocation")

        # flag is True if it is a mine, False otherwise
        # mines is the total number of mines around if it is a mine
        # parameters for checkLocation(x, y)
        flag, mines = gameBoard.checkLocation(coord[1], coord[0])

        # Chosen cell is a mine. Update kb and continue
        if flag:
            blownUp += 1
            new = {"cellLocation" : coord, "cellStatus" : 'M', "cellClue" : -1, "numOpen" : -1, "numMines" : -1, "numHidden" : -1}
            
            # Update cell value in the knowledge base
            for cell in kb:
                if cell['cellLocation'] == coord:
                    cell.update(new)
                    break

            # Given that we discovered a mine, we must update every neighbor of this mine 
            # that is searched.
            # With the discovery of a mine, we will be updating:
            # numOpen, numMines, numHidden
            updateNeighborsInKB('M', coord, kb)

            # Now we need to check if we can add more data into the kb given
            # the change in numMines and numHidden
            discoverHiddenNeighborsForKB(gameBoard, kb)
        # Our chosen cell is a safe cell
        else:
            # chosen cell is a safe
            # and we also have its number of mines in the variable 'mines'
            numOpN = getOpenNeighbors(coord, kb)
            new = {"cellLocation" : coord, "cellStatus" : 'S', "cellClue" : mines, "numOpen" : numOpN, "numMines" : 0, "numHidden" : kindOfCell(coord, dim) - numOpN}
            for cell in kb:
                if cell['cellLocation'] == coord:
                    cell.update(new)
                    break
            
            updateNeighborsInKB('S', coord, kb)
            discoverHiddenNeighborsForKB(gameBoard, kb)
            
    print("Opened ", dim*dim, " cells. Blown up: ", blownUp, " times out of ", blownUp+flagged," total mines.")
    return
            

            

# This function calculates hidden neighbors for safe cells in the kb
# if hidden neighbors are added to the kb
# First, update the safe cell's numOpen, numMines, and numHidden
# if the hidden neighbors are mines, then add to search and knowledgebase so that they are not queried 
# else all hidden neighbors are safes so add them to kb but don't add them to search yet
# Infering is continued until it cannot be done anymore         
def discoverHiddenNeighborsForKB(gameBoard, kb):
    infer = True
    while infer:
        kbincreased = False
        for cell in kb:
            # we are only updating safe cells
            if(cell['cellStatus'] == 'S'):
                check = 0
                flag, mines = gameBoard.checkLocation(cell['cellLocaton'][0],cell['cellLocaton'][1])
                # Every hidden neighbor is a mine
                if((cell['numHidden']  != 0) and (mines - cell['numMines'] == cell['numHidden'])):
                    check = 1
                
                # Every hidden neighbor is a safe
                if((cell['numHidden']  != 0) and ((kindOfCell(cell['cellLocation']) - mines) - cell['numOpen'] == cell['numHidden'])):
                    check = 2

                if(check != 0):
                    x = cell['cellLocation'][0]
                    y = cell['cellLocation'][1]

                    checkList = [[y+1,x],[y-1,x],[y,x+1],[y,x-1],[y+1,x+1],[y+1,x-1],[y-1,x+1],[y-1,x-1]]

                    while checkList:
                        c = checkList.pop()
                        
                        # all hidden neighbors are mines so add them to knowledge base and search 
                        if check == 1:
                            if((x >= 0 and x < dim-1) and (y >= 0 and y < dim -1)):
                                # Check that this hidden neighbor mine is not already in kb
                                if(not(inKB(c,kb))):
                                    new = {"cellLocation" : c, "cellStatus" : 'M', "cellClue" : -1, "numOpen" : -1, "numMines" : -1, "numHidden" : -1}
                                    kb.append(new)
                                    searched.append(c)
                                    kbincreased = True
                                    updateNeighborsInKB('M', c, kb)
                        else:
                            if((x >= 0 and x < dim-1) and (y >= 0 and y < dim -1)):
                                # Check that this hidden safe neighbor is not already in kb
                                if(not(inKB(c,kb))):
                                    # For this safe sell we must find the number of mines that this has
                                    f, m = gameBoard.checkLocation(c[0], c[1])
                                    # we have to look on to the kb to find the number of open neighbors
                                    numOpen = getOpenNeighbors(c,kb)
                                    numMines = m
                                    numHidden = kindOfCell(c,board.dim) - numOpen
                                    new = {"cellLocation" : c, "cellStatus" : 'S', "cellClue" : cellClue, "numOpen" : numOpen, "numMines" : numMines, "numHidden" : numHidden}
                                    kb.append(new)
                                    kbincreased = True
                                    updateNeighborsInKB('S', c, kb)
                    if not(kbincreased):
                        infer = False
    return

def inKB(coord, kb):
    for cell in kb:
        if(cell['cellLocation'] == coord):
            return True
    return False

def getOpenNeighbors(c,kb):
    x = coord[0]
    y = coord[1]

    numOpen = 0
    
    # all possible neighbor coords
    checkList = [[y+1,x],[y-1,x],[y,x+1],[y,x-1],[y+1,x+1],[y+1,x-1],[y-1,x+1],[y-1,x-1]]
    for cell in kb:
        if cell['cellLocation'] in checkList:
            numOpen +=1
    return numOpen

# The addition of a new cell forces us to update all of the neighbors in the kb
# This function will return the name of open neighbor cells
def updateNeighborsInKB(status, coord, kb):
    x = coord[0]
    y = coord[1]

    # all possible neighbor coords
    checkList = [[y+1,x],[y-1,x],[y,x+1],[y,x-1],[y+1,x+1],[y+1,x-1],[y-1,x+1],[y-1,x-1]]
    for cell in kb:
        if(cell['cellStatus'] == 'S'):
            if(cell['cellLocation'] in checkList):
                cell['numOpen'] +=1
                cell['numHidden'] -=1
                if(status == 'M'):
                    cell['numMines'] +=1
            
    return

# There are 3 types of cell that we can encounter in the board
# Corner cell (3 max neighbors)
# Cell along border of the board but is not a corner cell (5 max neighbors)
# Cell everywhere else in the board (8 max neighbors)
# This will return the max number of neighbors that this cell can have
def kindOfCell(coord, dim):
    x = coord[0]
    y = coord[1]
    
    # corner cell
    if((x == 0 and y == 0) or (x == 0 and y == dim-1) or (x == dim-1 and y == 0) or (x == dim -1 and y == dim-1)):
        return 3
    # border cell
    else if((x >= 0 and y == 0) or (x == 0 and y >= 0) or (x >= 0 and y == dim-1) or (x == dim-1 and y >= 0)):
        return 5
    # everywhere else cell
    else:
        return 8
