from gameboard import GameBoard
import random

# Part 2.1
# Given a board, will iterate over and do the basic algorithm as proviced in part 2.1
def improvedAlgo(gameBoard):

    board = gameBoard.board
    dim = len(board[0])

    blownUp = 0
    flagged = 0
    t = 0

    """
    List of dictionary values for knowledge base.
    Each cell has:
        cellLocation    <- [y,x] location of cell
        cellStatus      <- 'C' for covered, 'S' for safe, 'M' for mine
        cellClue        <- Once cell is opened, this is the number of mines around it
        numOpen         <- Number of squares identified around it
        numMines        <- Number of mines identified around it
        numHidden       <- Number of hidden squares around it
    """
    kb = boardInit(board)

    #printTerminalBoard(board)
    # List of already searched cells
    searched = []

    # Main loop. Each cell will be evalueted once
    i = 0
    while True:
    
        # searched list represents all the cells we have looked at. Once all cells have been either
        # searched or flagged as a mine, we can stop
        if (len(searched) >= dim*dim):
            break

        chosenCell = None
        randomCheck = True

        # Pick a safe cell to search from KB that hasn't already been searched.
        for cell in kb:
            cellStatus = cell.get("cellStatus")
            cellLocation = cell.get("cellLocation")
            if (cellStatus == 'S') and (cellLocation not in searched):
                chosenCell = cell
                searched.append(cellLocation)
                randomCheck = False
                break


        # There are no safe cells available to us. Choose one according to random function
        if(randomCheck):
            chosenCell = randomCellPick(kb, searched)
                


        # Have cell we will explore, call the environment to give us the information about the cell
        coord = chosenCell.get("cellLocation")

        flag, mines = gameBoard.checkLocation(coord[1], coord[0])
        
        # Chosen cell is a mine. Update kb and continue
        if flag:
            blownUp += 1
            new = {"cellLocation" : coord, "cellStatus" : 'M', "cellClue" : -1, "numOpen" : -1, "numMines" : -1, "numHidden" : -1}
            
            # Updte kb value
            for cell in kb:
                if cell['cellLocation'] == coord:
                    cell.update(new)
                    break
            i += 1
            continue
        else:

            # Not a mine, update relevant information
            numOpen, numMines, numHidden = checkCell(coord,kb,dim)

            new = {"cellLocation" : coord, "cellStatus" : 'S', "cellClue" : mines, "numOpen" : numOpen, "numMines" : numMines, "numHidden" : numHidden}
            
            # Updte kb value
            for cell in kb:
                if cell['cellLocation'] == coord:
                    cell.update(new)
                    break


            check = 0

            # Every hidden neighbor is a mine. Mark cellStatus as 'M', add it to searched list
            # so we do not query it.
            if (numHidden != 0) and (mines - numMines == numHidden):
                #print(coord[0],coord[1], " : ", mines, " : ", numMines, " : ", numHidden)
                check = 1

            # Every hidden neighbor is safe. Mark cellStatus as 'S', NOT adding it to searched list
            if (numHidden != 0) and ((8 - mines) - numOpen == numHidden):
                check = 2

            if (check != 0):
                y = coord[0]
                x = coord[1]
                checkList = [[y+1,x],[y-1,x],[y,x+1],[y,x-1],[y+1,x+1],[y+1,x-1],[y-1,x+1],[y-1,x-1]]

                while checkList:
                        c = checkList.pop()
                        
                        if check == 1: # Every hidden neighbor is a mine
                            for cell in kb:
                                if cell['cellLocation'] == c:
                                    if cell['cellStatus'] == 'C': # Hidden
                                        new = {"cellLocation" : c, "cellStatus" : 'M', "cellClue" : -1, "numOpen" : -1, "numMines" : -1, "numHidden" : -1}
                                        cell.update(new)
                                        searched.append(c)
                                        flagged += 1                        
                                        break
                        else:
                            for cell in kb:
                                if cell['cellLocation'] == c:
                                    if cell['cellStatus'] == 'C': # Hidden
                                        cellClue = cell.get('cellClue')
                                        numOpen = cell.get('numOpen')
                                        numMines = cell.get('numMines')
                                        numHidden = cell.get('numHidden')
                                        new = {"cellLocation" : c, "cellStatus" : 'S', "cellClue" : cellClue, "numOpen" : numOpen, "numMines" : numMines, "numHidden" : numHidden}
                                        cell.update(new)
                                        break

            else:
                """
                At this point we got past the basic checks and they did not find any easy cells to mark as mines/safe.
                Improved algorithm using inference of all surrounding cells.
                """
                checkImprove(kb)      

        i += 1
        
    print("Opened ", dim*dim, " cells. Blown up: ", blownUp, " times out of ", blownUp+flagged," total mines.")



# Given a cell, will return the number of hidden, mines, and open cells around it
# loc = cell location, kb = knowledgebase (list of dictionaries)
def checkCell(coord, kb, dim):

    y = coord[0]
    x = coord[1]

    numOpen = 0     # cellStatus = 'S'
    numMines = 0    # cellStatus = 'M'
    numHidden = 0   # cellStatus = 'C'

    # All possible cells we need to check
    check = [[y+1,x],[y-1,x],[y,x+1],[y,x-1],[y+1,x+1],[y+1,x-1],[y-1,x+1],[y-1,x-1]]

    while check:
        c = check.pop()
        
        for cell in kb:
            if cell['cellLocation'] == c:
                if cell['cellStatus'] == 'S':
                    numOpen += 1
                elif cell['cellStatus'] == 'M':
                    numMines += 1
                else:
                    numHidden += 1
                break

    return numOpen, numMines, numHidden


# Will initialize the knowledge base information for the board. Every cell starts out covered with nothing known about them.
def boardInit(board):

    kb = []
    dim = len(board[0])

    for i in range(dim):
        for j in range(dim):

            cellLocation = [i,j]
            cellStatus = 'C'
            cellClue = -1
            numOpen = -1
            numMines = -1
            numHidden = -1

            kb.append({"cellLocation" : cellLocation, "cellStatus" : cellStatus, "cellClue" : cellClue, "numOpen" : numOpen, "numMines" : numMines, "numHidden" : numHidden})

    return kb

def printKB(kb):
    for cell in kb:
        print(cell)


def randomCellPick(kb, searched):
    return 0

# Always start from top left and go down. There can never be a time where when we are infering, the cells we are infering have unknown
# to the left of them.
def checkImprove(kb):
    pass